import logging
import pytest
import random
import string

log = logging.getLogger(__name__)


@pytest.fixture
def hub(hub):
    hub.pop.sub.add("idem.idem")
    yield hub


@pytest.fixture(scope="module")
def instance_name():
    yield "test_idem_cloud_" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )


@pytest.fixture
def acct_subs():
    return ["vultr"]


@pytest.fixture
def acct_profile():
    return "test_development_idem_vultr"
