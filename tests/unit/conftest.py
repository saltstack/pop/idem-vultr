import pytest


@pytest.fixture
def hub(hub):
    hub.pop.sub.add("idem.idem")
    return hub
